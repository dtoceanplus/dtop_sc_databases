# DTOceanPlus Site Characterisation (SC) module databases

This repository contains DTOceanPlus Site Characterisation module databases used by SC module.

Please see `.md' documentation files of [DTOP Installation repo](https://gitlab.com/dtoceanplus/dtop_inst).

**NOTE**:

DTOP SC module needs and uses the set of own basic data files.
These SC data files are provided in the quite large archive `dtop_inst_sc_databases_v1.x.tar.gz`.
The archive should be downloaded separately and copied into DTOP installation directory to be used by the installation procedure.

The exact version of the used SC data files archive is defined in `dtop_inst.env` with the variable `SC_databases_archive_version`, for example :
```
SC_databases_archive_version=dtop_inst_sc_databases_v1.5.tar.gz
```
